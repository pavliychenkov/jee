<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <title> Upload From Library =) </title>
</head>
<body>
<%--header of table--%>
<table>
    <tr>
        <td style="width: 100px"><b>Name</b></td>
        <td style="width: 100px"><b>Author</b></td>
        <td style="width: 150px"><b>Description</b></td>
        <td style="width: 150px"><b>Cover</b></td>
    </tr>
    <tr>
        <td style="width: 100px"><c:out value="${book.name} "/></td>
        <td style="width: 100px"><c:out value="${book.author} "/></td>
        <td style="width: 150px"><c:out value="${book.description} "/></td>
        <td style="width: 150px">
            <img src="/upload/${requestScope.book.id}.jpg" height="50px" width="30px" alt="here should be picture :("/>
        </td>
    </tr>
</table>

<%-- book info --%>
<a href="<c:url value="/options/chooseBookController?id=${book.id}&action=edit"/>">edit task</a>
<a href="<c:url value="/options/deleteBookController?id=${book.id}&action=delete"/>">delete</a>
<a href="<c:url value="/options/getBooksController"/>"> return </a>

<%-- upload part--%>
<form action="/uploadController" method="post" enctype="multipart/form-data">
    <input type="file" name="file">
    <input type="hidden" name="id" value="${book.id}">
    <input type="hidden" name="book" value="${book}">
    <a href="javascript:" onclick="parentNode.submit();">[upload]</a>
</form>

<input type="hidden" name="id" value="${book.id}">
<br>
<c:if test="${not empty requestScope.isEdit}">
    <form action="/options/editBookController" method="post">
        <label id="txtAuthor">Book Author: </label>
        <br>
        <textarea name="author" required>${book.author}</textarea>
        <br>
        <label id="txtDescription">Book description: </label>
        <br>
        <textarea name="description" required>${book.description}</textarea>
        <br>
        <input type="hidden" name="id" value="${book.id}">
        <a href="javascript:" onclick="parentNode.submit();">save</a>
    </form>
</c:if>
</body>
</html>
