package filters;

import wrappers.BookPageWrapper;
import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;
import java.util.logging.Logger;


public class UploadFilter implements Filter {
    private final static Logger LGGR = Logger.getLogger(UploadFilter.class.getName());
    private final static String PART_UPLOAD = "/uploadController";

    @Override
    public void init(final FilterConfig filterConfig) {

    }

    @Override
    public void doFilter(final ServletRequest servletRequest,
                         final ServletResponse servletResponse,
                         final FilterChain filterChain)
            throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        final String reqURI = req.getRequestURI();

        if (reqURI.contains(PART_UPLOAD)) {
            LGGR.info(req.getLocalName() + " - local name, "
                    + req.getServletPath() + " - servlet path");
        }
        filterChain.doFilter(
                new BookPageWrapper((HttpServletRequest) servletRequest),
                servletResponse);
    }

    @Override
    public void destroy() {

    }
}
