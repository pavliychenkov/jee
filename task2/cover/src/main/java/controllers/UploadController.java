package controllers;

import utils.GetProperties;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import java.io.File;
import java.io.IOException;
import java.util.logging.Logger;

@MultipartConfig()
@WebServlet(name = "UploadController",
        value = "/uploadController")
public class UploadController extends HttpServlet {
    private final static Logger LGGR = Logger.getLogger(UploadController.class.getName());
    private final static String FILE_DIR = "upload" + "\\";
    private final static String KEY_FILE = "file";

    @Override
    public void init() {
    }

    @Override
    protected void doPost(final HttpServletRequest req,
                          final HttpServletResponse resp)
            throws ServletException, IOException {

        LGGR.info("used doPost method from UploadController");
        final String idBook = req.getParameter("id");
        final Part image = req.getPart(KEY_FILE);
        LGGR.info(idBook + " - - - - - - id book");

        if (idBook == null || "".equals(idBook)) {
            req.setAttribute("msg", "File can't to add");
            req.getRequestDispatcher("/options/index.jsp").forward(req, resp);
            LGGR.info("id book or mime type is not valid");
            return;
        }

        final String realPathWithDir = req.getServletContext().getRealPath("")
                + FILE_DIR;
        final String fileName = image.getSubmittedFileName();
        final String fileNameWithIdBook = changeNameToIdBook(fileName, idBook);

        final File dir = new File(realPathWithDir);
        if (!dir.exists()) {
            dir.mkdir();
        }
        final String mimeType = image.getContentType();

        if (mimeType.equals(GetProperties.get("jpeg"))) {
            final String fullPathFile = realPathWithDir + fileNameWithIdBook;
            image.write(fullPathFile);
            req.setAttribute("msg", "file added success");
            LGGR.info("file added success");
        } else {
            req.setAttribute("msg",
                    "type file can't to added, please use only jpg or jpeg files");
            LGGR.info("mime type is not valid");
        }
        req.getRequestDispatcher("/options/chooseBookController")
                .forward(req, resp);
    }

    /**
     * change file name to name{bookId.postfix}
     *
     * @param fileName
     * @param idBook
     * @return
     */
    private String changeNameToIdBook(final String fileName,
                                      final String idBook) {
        return idBook + "." + getPostfix(fileName);
    }

    /**
     * get String postfix file, example: file = 123.exe, return "exe"
     *
     * @param fileName
     * @return
     */
    private String getPostfix(final String fileName) {
        final String[] result = fileName.split("\\.");
        return result[result.length - 1];
    }

}

