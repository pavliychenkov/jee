package wrappers;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;

/**
 * The type Book page wrapper.
 */
public class BookPageWrapper extends HttpServletRequestWrapper {
    private final static String FAKE_PATH = "/WEB-INF/Upload.jsp";

    /**
     * Instantiates a new Book page wrapper.
     *
     * @param request the request
     */
    public BookPageWrapper(HttpServletRequest request) {
        super(request);
    }

    @Override
    public void setAttribute(final String name, final Object o) {
        if ("path".equals(name)) {
            super.setAttribute("path", FAKE_PATH);
        } else {
            super.setAttribute(name, o);
        }
    }
}
