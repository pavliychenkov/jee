<%@ page import="model.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <title>Task3</title>
</head>
<body>
You are logged in as <b> <c:out value="${user.firstName}"/> </b>
<div style="margin-left: 35%">
    <c:import url="<%=Constants.JSP_MENU%>"/>
    <br>

    <%--show the list books--%>
    <c:if test="${not empty books}">

        <%--Header of table--%>
        <table>
            <tr>
                <td style="width: 100px"><b>Name</b></td>
                <td style="width: 100px"><b>Author</b></td>
                <td style="width: 150px"><b>Description</b></td>
                <td style="width: 50px"><b>Options </b></td>
            </tr>
        </table>

        <%--content table--%>
        <c:forEach items="${books}" var="book">
            <table>
                <tr>
                    <td style="width: 100px"><c:out value="${book.name} "/></td>
                    <td style="width: 100px"><c:out value="${book.author} "/></td>
                    <td style="width: 150px"><c:out value="${book.description} "/></td>
                    <td style="width: 50px">
                        <a href="<c:url value="<%=Constants.SERVLET_VALUE_CHOOSE_BOOK%>"/>?id=${book.id}">edit</a>
                    </td>
                </tr>
            </table>
            <br>
        </c:forEach>
    </c:if>

    <%-- show form for add new book--%>
    <c:if test="${not empty param.flag}">
        <c:import url="<%=Constants.JSP_ADD_NEW_BOOK%>"/>
    </c:if>

    <%-- show special page from menu link--%>
    <c:if test="${not empty param.special}">
        <c:import url="<%=Constants.JSP_SPECIAL_PAGE%>"/>
    </c:if>

    <%-- show special form from servlet async--%>
    <c:if test="${not empty requestScope.special}">
        <c:import url="<%=Constants.JSP_SPECIAL_PAGE%>"/>
    </c:if>

    <%-- show book page --%>
    <c:if test="${not empty requestScope.book}">
        <c:import url="<%=Constants.JSP_BOOK_PAGE%>"/>
    </c:if>

</div>
</body>
</html>
