<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page import="model.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <title>Add new book</title>
</head>
<body>
    <form action="<%=Constants.SERVLET_VALUE_ADD_BOOK%>" method="post">
        <label id="txtName" >Book name: </label><br>
        <textarea name="name" required></textarea><br>
        <label id="txtAuthor" >Book Author: </label><br>
        <textarea name="author" required></textarea><br>
        <label id="txtDescription" >Book description: </label><br>
        <textarea name="description" required></textarea><br>
        <a href="javascript:" onclick="parentNode.submit();">add new book</a>
        <a href="<c:url value="<%=Constants.SERVLET_VALUE_GET_BOOKS%>"/>">return</a>
    </form>
</body>
</html>
