<%@ page import="model.Constants" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<p style="color: red"><c:out value="${msg}"/></p><br>
<a href="<c:url value="<%=Constants.SERVLET_VALUE_GET_BOOKS%>"/>">[show all books]</a>
<a href="<c:url value="<%=Constants.JSP_INDEX%>"/>?flag=flag"> [add new book]</a>
<a href="<c:url value="<%=Constants.JSP_INDEX%>"/>?special=special">[special controller]</a>
<a href="<c:url value="<%=Constants.SERVLET_VALUE_LOGOUT%>"/>" style="color: red">[logout]</a>