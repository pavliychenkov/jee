<%@ page import="model.Constants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <title>Special Page</title>
</head>
<body>
    <form action="<%=Constants.SERVLET_VALUE_SPECIAL_CONTROLLER%>" method="get">
        <p>Please input number for get degree squared and degree cube</p>
        <input type="number" name="valueOne">
        <a href="javascript:" onclick="parentNode.submit();">[calculate]</a>
    </form>
    <c:if test="${not empty requestScope.result}">
        <c:out value="Results of number = ${requestScope.number}; "/> <br>
        <c:out value="square = ${requestScope.result}"/> <br>
        <c:out value="cube = ${requestScope.result2}"/> <br>
    </c:if>
    <a href="<%=Constants.SERVLET_VALUE_GET_BOOKS%>">return</a>
</body>
</html>