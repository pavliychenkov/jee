<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <title>Register Page</title>
</head>
<body>

<div style="margin-left: 35%">
<p style="color: red"><c:out value="${msg}"/></p><br>
    <form action="/registerController" method="post">
        <label id="login">Login</label> <br>
        <input type="text" name="login" required></input> <br>
        <label id="password">Password</label><br>
        <input type="password" name="password" required ></textarea><br>
        <label id="firstName">first name</label><br>
        <input type="firstName" name="firstName" required ></textarea><br>
        <label id="secondName">second name</label><br>
        <input type="secondName" name="secondName" required ></textarea><br>

        <a href="<c:url value="login.jsp"/>" >[ login ]</a>
        <a href="javascript:" onclick="parentNode.submit();">[ register ]</a>
    </form>
</div>
</body>
</html>
