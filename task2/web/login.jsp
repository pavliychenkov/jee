<%@ page import="model.Constants" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <link rel="stylesheet" href="css/style.css">
    <title>Login Page</title>
</head>
<body>
<div style="margin-left: 35%">

<p style="color: red"><c:out value="${msg}"/></p><br>
    <form action="/loginController" method="post">
        <label id="login">Login</label> <br>
        <input type="text" name="login" required/> <br>
        <label id="password">Password</label><br>
        <input type="password" name="password" required ></textarea><br>
        <a href="javascript:" onclick="parentNode.submit();">[ login ]</a>
        <a href="/register.jsp">[ register ]</a>
    </form>
</div>
</body>
</html>