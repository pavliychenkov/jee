package model.dao;

import model.beans.Book;
import model.interfaces.IBookDAO;
import model.service.BDManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Book dao jdbc.
 */
public class BookDAOJDBC implements IBookDAO {
    private static final Logger LOGGER =
            Logger.getLogger(BookDAOJDBC.class.getName());
    private BDManager bdManager = new BDManager();

    private static final String SQL_GET_LIST_BOOKS = "select * from tasksshema.books where userId = ";
    private static final String SQL_DELETE_BOOK = "delete from tasksshema.books where id = ?;";
    private static final String SQL_ADD_BOOK = "insert into tasksshema.books (" +
            Book.NAME + ", " +
            Book.AUTHOR + ", " +
            Book.DESCRIPTION + ", " +
            Book.USER_ID + ") " +
            "values (?, ?, ?, ?)";
    private static final String SQL_UPDATE_DESCRIPTION_BOOK = "update tasksshema.books set description = " +
            "?" +
            " where id = " +
            "?" + ";";
    private static final String SQL_UPDATE_AUTHOR_BOOK = "update tasksshema.books set author = " +
            "?" +
            " where id = " +
            "?" + ";";
    private static final String SQL_GET_BOOK = "select * from tasksshema.books where id = ";

    @Override
    public List<Book> getListBooks(Integer userId) {
        List<Book> books = new ArrayList<>();
        bdManager = new BDManager();

        try (PreparedStatement ps = bdManager.getStatement(
                SQL_GET_LIST_BOOKS + userId + ";");
             ResultSet rs = ps.executeQuery()) {
            while (rs.next()) {
                Integer id = rs.getInt(Book.ID);
                String name = rs.getString(Book.NAME);
                String author = rs.getString(Book.AUTHOR);
                String description = rs.getString(Book.DESCRIPTION);
                books.add(new Book(id, name, author, description));
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return books;
    }

    @Override
    public boolean addBook(final Book book) {
        final int BOOK_NAME = 1;
        final int BOOK_AUTHOR = 2;
        final int BOOK_DESCRIPTION = 3;
        final int ID_USER = 4;

        bdManager = new BDManager();

        try (PreparedStatement ps = bdManager.getStatement(
                SQL_ADD_BOOK)) {
            ps.setString(BOOK_NAME, book.getName());
            ps.setString(BOOK_AUTHOR, book.getAuthor());
            ps.setString(BOOK_DESCRIPTION, book.getDescription());
            ps.setInt(ID_USER, book.getUserId());
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public boolean updateDescription(final Integer id, final String description) {
        final int DESCRIPTION = 1;
        final int ID = 2;
        bdManager = new BDManager();
        try (PreparedStatement ps = bdManager.getStatement(
                             SQL_UPDATE_DESCRIPTION_BOOK)) {
            ps.setString(DESCRIPTION, description);
            ps.setInt(ID, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }


    @Override
    public boolean updateAuthor(final Integer id, final String author) {
        final int AUTHOR = 1;
        final int ID = 2;
        bdManager = new BDManager();
        try (PreparedStatement ps = bdManager.getStatement(
                SQL_UPDATE_AUTHOR_BOOK)) {
            ps.setString(AUTHOR, author);
            ps.setInt(ID, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }

    @Override
    public Book getBook(final Integer id) {
        Book book = null;
        bdManager = new BDManager();
        try (PreparedStatement ps = bdManager.getStatement(
                SQL_GET_BOOK + id + ";");
             ResultSet rs = ps.executeQuery()) {
            book = new Book();
            while (rs.next()) {
                book.setId(rs.getInt(Book.ID));
                book.setName(rs.getString(Book.NAME));
                book.setAuthor(rs.getString(Book.AUTHOR));
                book.setDescription(rs.getString(Book.DESCRIPTION));
                return book;
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return null;
    }

    @Override
    public boolean deleteBook(final Integer id) {
        final int ID = 1;
        bdManager = new BDManager();
        try (PreparedStatement ps =
                     bdManager.getStatement(SQL_DELETE_BOOK)) {
            ps.setInt(ID, id);
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            return false;
        }
        return true;
    }
}