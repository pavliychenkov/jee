package model.dao;

import model.beans.User;
import model.interfaces.IUserDAO;
import model.service.BDManager;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class UserDAOJDBC implements IUserDAO {
    private static final Logger LOGGER = Logger.getLogger(UserDAOJDBC.class.getName());
    private static final BDManager bdManager = new BDManager();

    private static final String SQL_REGISTER_USER = "insert into tasksshema.bookusers (login, password, firstName, secondName) values (?, ?, ?, ?)";
    private final static String SQL_GET_USER = "select id, firstName, secondName from tasksshema.bookusers where login = ? && password = ? ;";

    @Override
    public User getUser(final String login, final String password) {
        final int LOGIN = 1;
        final int PASSWORD = 2;
        User user = null;
        try (PreparedStatement ps = bdManager.getStatement(SQL_GET_USER)) {
            ps.setString(LOGIN, login);
            ps.setString(PASSWORD, password);
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                user = new User();
                user.setId(rs.getInt(User.ID));
                user.setFirstName(rs.getString(User.FIRST_NAME));
                user.setSecondName(rs.getString(User.SECOND_NAME));
                return user;
            }
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return user;

    }

    @Override
    public User registerUser(final String login, final String password,
                             final String firstName, final String secondName) {
        final int LGN = 1;
        final int PSW = 2;
        final int F_NAME = 3;
        final int S_NAME = 4;

        final User user = getUser(login, password);
        if (user != null){
            return null;
        }

        try (PreparedStatement ps = bdManager.getStatement(
                SQL_REGISTER_USER)) {
            ps.setString(LGN, login);
            ps.setString(PSW, password);
            ps.setString(F_NAME, firstName);
            ps.setString(S_NAME, secondName);
            ps.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
            return null;
        }
        return new User(firstName, secondName);
    }
}
