package model.interfaces;

import model.beans.User;

public interface IUserDAO {
    User getUser(String login, String password);
    User registerUser(String login, String password, String firstName, String secondName);

}
