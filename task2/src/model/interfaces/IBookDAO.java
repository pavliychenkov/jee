package model.interfaces;

import model.beans.Book;

import java.util.List;

/**
 * The interface Book dao.
 */
public interface IBookDAO {
     /**
      * Gets list books.
      *
      * @return the list books
      */
     List<Book> getListBooks(Integer userId);

     /**
      * Gets book.
      *
      * @param id the id
      * @return the book
      */
     Book getBook(Integer id);

     /**
      * Add book.
      *
      * @param book the book
      */
     boolean addBook(Book book);

     /**
      * Update description.
      *
      * @param id          the id
      * @param description the description
      */
     boolean updateDescription(Integer id, String description);

     /**
      * Update author.
      *
      * @param id     the id
      * @param author the author
      */
     boolean updateAuthor(Integer id, String author);

     /**
      * Delete book.
      *
      * @param id the id
      */
     boolean deleteBook(Integer id);

}
