package model.factories;

import model.dao.UserDAOJDBC;
import model.interfaces.IUserDAO;

import java.util.HashMap;
import java.util.Map;

public class UserDAOFactory {
    private static Map<String, IUserDAO> userDAOMap = new HashMap<>();

    static {
        userDAOMap.put(UserDAOJDBC.class.getName(), new UserDAOJDBC());
    }

    public static IUserDAO getDAO(final String nameDAO){
        return userDAOMap.get(nameDAO);
    }
}
