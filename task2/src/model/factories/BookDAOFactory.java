package model.factories;

import model.dao.BookDAOJDBC;
import model.interfaces.IBookDAO;

import java.util.HashMap;
import java.util.Map;

/**
 * The type Book dao factory.
 */
public class BookDAOFactory {
    private static Map<String, IBookDAO> mapBookDAO = new HashMap<>();

    static {
        mapBookDAO.put(BookDAOJDBC.class.getName(), new BookDAOJDBC());
    }

    /**
     * Get book dao book dao.
     *
     * @param nameDAO the name dao
     * @return the book dao
     */
    public static IBookDAO getBookDAO(final String nameDAO) {
        return mapBookDAO.get(nameDAO);
    }
}
