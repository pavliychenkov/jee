package model;

import model.dao.BookDAOJDBC;
import model.dao.UserDAOJDBC;
import model.factories.BookDAOFactory;
import model.service.GetProperties;

public class Constants {
    /**DAO*/
    public static final String BOOK_DAO = BookDAOJDBC.class.getName();
    public static final String USER_DAO = UserDAOJDBC.class.getName();
    public static final String DAO_BOOK_FACTORY = BookDAOFactory.class.getName();

    /**Keys */
    public static final String KEY_ACTION = "action";
    public static final String KEY_NAME = "name";
    public static final String KEY_AUTHOR = "author";
    public static final String KEY_DESCRIPTION = "description";
    public static final String KEY_BOOKS = "books";
    public static final Object KEY_EDIT = "edit";
    public static final Object KEY_RETURN = "return";
    public static final String KEY_ERROR = "error";
    public static final String KEY_MSG = "msg";
    public static final String KEY_LOGIN = "login";
    public static final String KEY_PASSWORD = "password";
    public static final String KEY_DAO_BOOK = "DAO_BOOK";
    public static final String KEY_DAO_USER = "DAO_USER";
    public static final String KEY_USER = "user";
    public static final String KEY_IS_EDIT = "isEdit";
    public static final String KEY_SESSION_ID = "sessionId";
    public static final String KEY_RESULT_TWO = "result2";
    public static final String KEY_NUMBER = "number";
    public static final String KEY_RESULT = "result";
    public static final String KEY_SPECIAL = "special";

    /**servlet names*/
    public static final String SERVLET_NAME_BASIC = "GetBooksController";
    public static final String SERVLET_NAME_CHOOSE_BOOK = "ChooseBookController";
    public static final String SERVLET_NAME_EDIT_BOOK = "EditBookController";
    public static final String SERVLET_NAME_ADD_BOOK = "AddNewBookController";
    public static final String SERVLET_NAME_DELETE_BOOK = "DeleteBookController";
    public static final String SERVLET_NAME_LOGIN = "LoginController";
    public static final String SERVLET_NAME_REGISTER_USER = "RegisterController";
    public static final String SERVLET_NAME_LOGOUT = "LogoutController";
    public static final String SERVLET_NAME_SPECIAL_CONTROLLER = "SpecialController";
    public static final String SERVLET_NAME_CALC_CUBE = "SpecialTwoController";

    /**servlet values*/
    public static final String SERVLET_VALUE_GET_BOOKS = "/options/getBooksController";
    public static final String SERVLET_VALUE_CHOOSE_BOOK = "/options/chooseBookController";
    public static final String SERVLET_VALUE_EDIT_BOOK = "/options/editBookController";
    public static final String SERVLET_VALUE_ADD_BOOK = "/options/addNewBookController";
    public static final String SERVLET_VALUE_DELETE_BOOK = "/options/deleteBookController";
    public static final String SERVLET_VALUE_LOGIN = "/loginController";
    public static final String SERVLET_VALUE_REGISTER_USER = "/registerController";
    public static final String SERVLET_VALUE_LOGOUT = "/logoutController";
    public static final String SERVLET_VALUE_SPECIAL_CONTROLLER = "/options/specialController";
    public static final String SERVLET_VALUE_CALC_CUBE = "/options/specialTwoController";

    /**
     * jsp  pages
     */
    public static final String JSP_ADD_NEW_BOOK = "addNewBook.jsp";
    public static final String JSP_BOOK_PAGE = "BookPage.jsp";
    public static final String JSP_LOGIN_PAGE = "login.jsp";
    public static final String JSP_INDEX = "/options/index.jsp";
    public static final String JSP_PAGE_REGISTER = "register.jsp";
    public static final String JSP_SPECIAL_PAGE = "/options/special.jsp";
    public static final String JSP_MENU = "menu.jsp";


    /**settings*/
    public static boolean FLAG_UNAVAILABILITY = false;                 //permanent or some time unavailable
    public static boolean FLAG_AVAILABILITY_PERMANENT = false;         //permanent or some time unavailable
    public static int     TIME_UNAVAILABLE = 3;                      //how long servlet unavailable

    /**values for bd connection*/
    public static final String BD_CLASS_NAME = GetProperties.get("db.class_name");
    public static final String DB_URL = GetProperties.get("db.url");
    public static final String DB_USER = GetProperties.get("db.user");
    public static final String DB_PSWRD = GetProperties.get("db.password");

    /** error messages */
    public static final String ERR_MSG_NOT_AVAILABLE_PERM = "This resource in not available permanently";
    public static final String ERR_MSG_SESSION_IS_CLOSED = "Sorry but session is closed";
    public static final String ERR_MSG_INPUT_DATA_EMPTY = "you must enter all fields, space is not valid";
    public static final String ERR_MSG_NOT_AVAILABLE_TIME = "Resource will not is available still - ";
    public static final String ERR_MSG_XSS_WRAPPER = "identified attempt of attack - < script >";
    public static final String ERR_MSG_INVALID_UPDATE_BOOK = "author or description is empty or have invalid information, pls try again";
    public static final String ERR_MSG_INVALID_INPUT_ADD_BOOK = "name, author or description is empty or have invalid information, pls try again";
    public static final String ERR_MSG_INVALID_LOGIN_OR_PASSWORD = "login or password incorrect";

    /** messages */
    public static final String MSG_SUCCESS_BOOK_UPDATED = "Book is updated successful";
    public static final String MSG_LOGOUT = "You logout";
    public static final String MSG_AVAILABLE_SERVLET = "All available servlets";
    public static final String MSG_PLS_INPUT_NUMBER = "Please input number";
    public static final String MSG_HAVE_NOT_BOOK = "You haven't books, may you want add new book?";

    /**  paths*/
    public static final String PATH = "/";
}
