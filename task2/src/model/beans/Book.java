package model.beans;


import java.util.Objects;

public class Book {
    public static final String BOOK = "book";
    public static final String ID = "id";
    public static final String NAME = "name";
    public static final String AUTHOR = "author";
    public static final String DESCRIPTION = "description";
    public static final String USER_ID = "userId";

    private Integer id;
    private String name;
    private String author;
    private String description;
    private Integer userId;
    public Book() {
    }

    public Book(final String name, final String author, final String description) {
        this.name = name;
        this.author = author;
        this.description = description;
    }

    public Book(Integer id, String name, String author, String description, Integer userId) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.description = description;
        this.userId = userId;
    }

    public Book(final Integer id, final String name, final String author,
                final String description) {
        this.id = id;
        this.name = name;
        this.author = author;
        this.description = description;
    }

    public Book(String name, String author, String description, Integer userId) {
        this.name = name;
        this.author = author;
        this.description = description;
        this.userId = userId;
    }

    public Integer getId() {
        return id;
    }

    public void setId(final Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(final String name) {
        this.name = name;
    }

    public String getAuthor() {
        return author;
    }

    public void setAuthor(final String author) {
        this.author = author;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(final String description) {
        this.description = description;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    @Override
    public boolean equals(final Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }
        Book book = (Book) o;
        return Objects.equals(id, book.id) &&
                Objects.equals(name, book.name) &&
                Objects.equals(author, book.author) &&
                Objects.equals(description, book.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, author, description);
    }

    @Override
    public String toString() {
        return id + " " + name + " " + author + " " + description;
    }
}
