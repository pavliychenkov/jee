package model.service;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.logging.Logger;

/**
 * The type Get properties.
 */
public final class GetProperties {
    private final static Logger LOGGER = Logger.
            getLogger(GetProperties.class.getName());
    private final static String PATH_PROP = "c:\\git\\jee\\task2\\src\\"
            + "config.properties";
    private static Properties properties;

    private GetProperties() {
    }

    /**
     * Return value string from properties.
     *
     * @param key the key
     * @return the string
     */
    public static String get(final String key) {
        if (properties == null) {
            FileInputStream fileInputStream;
            properties = new Properties();
            try {
                fileInputStream = new FileInputStream(PATH_PROP);
                properties.load(fileInputStream);
            } catch (IOException e) {
                LOGGER.info(e.getMessage());
            }
        }
        return properties.getProperty(key);
    }

}
