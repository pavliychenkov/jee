package model.service;

import model.Constants;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * The type Bd connection.
 */
public class BDManager {
    private final Logger LOGGER = Logger.getLogger(BDManager.class.getName());
    private Connection connection;

    /**
     * Instantiates a new Bd connection.
     */
    public BDManager() {
        createConnection();
    }

    private Connection createConnection() {
        try {
            Class.forName(Constants.BD_CLASS_NAME);
            connection = DriverManager.getConnection(
                    Constants.DB_URL,
                    Constants.DB_USER,
                    Constants.DB_PSWRD);
        } catch (SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        } catch (ClassNotFoundException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return connection;
    }

    /**
     * Gets connection.
     *
     * @return the connection
     */
    public Connection getConnection() {
        return connection;
    }

    /**
     * Gets statement.
     *
     * @param sql the sql
     * @return the statement
     * @throws SQLException the sql exception
     */
    public PreparedStatement getStatement(final String sql) throws SQLException {
        return connection.prepareStatement(sql);
    }

}
