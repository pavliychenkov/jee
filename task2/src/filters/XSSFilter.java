package filters;

import wrappers.XSSWrapperRequest;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * The type Xss filter.
 */
public class XSSFilter implements Filter {
    @Override
    public void init(final FilterConfig filterConfig) throws ServletException {
    }

    @Override
    public void doFilter(
            final ServletRequest servletRequest,
            final ServletResponse servletResponse,
            final FilterChain filterChain)
            throws IOException, ServletException {
        filterChain.doFilter(
                new XSSWrapperRequest((HttpServletRequest) servletRequest),
                servletResponse
        );
    }

    @Override
    public void destroy() {

    }
}
