package controllers;

import model.Constants;
import model.beans.Book;
import model.beans.User;

import javax.servlet.ServletException;
import javax.servlet.UnavailableException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * The type Get books from BD controller.
 */
@WebServlet(value = Constants.SERVLET_VALUE_GET_BOOKS,
        name = Constants.SERVLET_NAME_BASIC,
        displayName = Constants.SERVLET_NAME_BASIC)
public class GetBooksController extends BaseController {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        showAllBooks(req, resp);
    }

    /**
     * Show all books from bd to BookPage.jsp.
     *
     * @param request
     * @param response
     * @throws ServletException
     * @throws IOException
     */
    protected void showAllBooks(
            final HttpServletRequest request,
            final HttpServletResponse response)
            throws ServletException, IOException {
        checkAvailable(request);

        User user = obtainUserFromRequest(request);
        List<Book> books = getBookDAO().getListBooks(user.getId());
        request.setAttribute(Constants.KEY_BOOKS, books);
        if (books.isEmpty()) {
            forward(request, response, Constants.JSP_INDEX,
                    Constants.MSG_HAVE_NOT_BOOK);
            return;
        }
        forward(request, response, Constants.JSP_INDEX);
    }

    /**
     * Check available.
     *
     * @param request the request
     * @throws UnavailableException the unavailable exception
     */
    protected void checkAvailable(
            final HttpServletRequest request) throws UnavailableException {
        if (Constants.FLAG_UNAVAILABILITY) {
            if (request.getServletPath().equals(
                    Constants.SERVLET_VALUE_GET_BOOKS)) {
                UnavailableException exception;
                if (Constants.FLAG_AVAILABILITY_PERMANENT) {
                    exception = new UnavailableException(
                            Constants.ERR_MSG_NOT_AVAILABLE_PERM);
                } else {
                    exception = new UnavailableException(
                            Constants.ERR_MSG_NOT_AVAILABLE_TIME
                                    + Constants.TIME_UNAVAILABLE,
                            Constants.TIME_UNAVAILABLE);
                }
                Constants.FLAG_UNAVAILABILITY = false;
                throw exception;
            }
        }
    }
}
