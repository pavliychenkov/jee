package controllers;

import model.Constants;
import model.beans.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.HttpConstraint;
import javax.servlet.annotation.ServletSecurity;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type Delete book controller.
 */

@WebServlet(name = Constants.SERVLET_NAME_DELETE_BOOK,
        value = Constants.SERVLET_VALUE_DELETE_BOOK)
public class DeleteBookController extends BaseController {

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        final String id = req.getParameter(Book.ID);
        final Integer idBook = Integer.parseInt(id);

        getBookDAO().deleteBook(idBook);
        forward(req, resp, Constants.SERVLET_VALUE_GET_BOOKS);
    }
}