package controllers;

import model.Constants;
import model.beans.User;
import model.factories.BookDAOFactory;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.*;
import java.io.IOException;

@WebServlet(name = Constants.SERVLET_NAME_LOGIN,
        value = Constants.SERVLET_VALUE_LOGIN)
public class LoginController extends BaseController {

    @Override
    public void init() {
    }

    @Override
    protected void doPost(final HttpServletRequest req,
                          final HttpServletResponse resp)
            throws ServletException, IOException {
        login(req, resp);
    }

    /**
     * login method
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void login(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException {

        final String login = req.getParameter(Constants.KEY_LOGIN);
        final String password = req.getParameter(Constants.KEY_PASSWORD);

        if (!Utils.checkOnNullOrEmptyString(login, password)) {
            forward(req, resp, Constants.JSP_LOGIN_PAGE,
                    Constants.ERR_MSG_INVALID_LOGIN_OR_PASSWORD);
            return;
        }

        final User user = getUserDAO().getUser(login, password);

        if (user != null) {
            req.getSession().setAttribute(Constants.KEY_USER, user);
            getServletContext().setAttribute(Constants.KEY_DAO_BOOK,
                    BookDAOFactory.getBookDAO(Constants.BOOK_DAO));
            forward(req, resp, Constants.JSP_INDEX, "Hello "
                    + user.getFirstName());
            return;
        } else {
            forward(req, resp, Constants.JSP_LOGIN_PAGE,
                    "User with current login and password was't founded");
            return;
        }
    }
}
