package controllers;

import model.Constants;
import model.beans.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type Choose book controller.
 */
@WebServlet(value = Constants.SERVLET_VALUE_CHOOSE_BOOK,
        name = Constants.SERVLET_NAME_CHOOSE_BOOK,
        displayName = Constants.SERVLET_NAME_CHOOSE_BOOK)
public class ChooseBookController extends BaseController {
    private final static String PATH_TO_JSP = Constants.JSP_BOOK_PAGE;

    @Override
    protected void doGet(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        chooseBook(req, resp);
    }
    @Override
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        chooseBook(req, resp);
    }

    /**
     * Choose book from BD.
     *
     * @param req  the req
     * @param resp the resp
     * @throws ServletException the servlet exception
     * @throws IOException      the io exception
     */
    protected void chooseBook(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {

        String action = req.getParameter(Constants.KEY_ACTION);
        String id = req.getParameter(Book.ID);
        int idBook = Integer.parseInt(id);

        Book book = getBookDAO().getBook(idBook);
        req.setAttribute(Book.BOOK, book);
        req.setAttribute("path", PATH_TO_JSP);

        if (Constants.KEY_EDIT.equals(action)) {
            req.setAttribute(Constants.KEY_IS_EDIT, Constants.KEY_IS_EDIT);
        }

        forward(req, resp, Constants.JSP_INDEX);
    }
}

