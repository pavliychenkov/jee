package controllers.async;

import com.mysql.cj.util.StringUtils;
import controllers.Utils;
import listeners.MyAsyncListener;
import model.Constants;

import javax.servlet.AsyncContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = Constants.SERVLET_NAME_SPECIAL_CONTROLLER,
        value = Constants.SERVLET_VALUE_SPECIAL_CONTROLLER,
        asyncSupported = true)
public class SpecialController extends HttpServlet {
    private final static String KEY_VALUE = "valueOne";
    @Override
    protected void doGet(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException {

        String firstArg = req.getParameter(KEY_VALUE);
        int firstResult = 0;

        if (StringUtils.isNullOrEmpty(firstArg)) {
            req.setAttribute(Constants.KEY_SPECIAL, Constants.KEY_SPECIAL);
            Utils.forward(req, resp, Constants.JSP_INDEX,
                    Constants.MSG_PLS_INPUT_NUMBER);
            return;
        } else {
            firstResult = Integer.parseInt(firstArg);
        }

        AsyncContext asyncContext = req.startAsync();
        asyncContext.addListener(new MyAsyncListener());

        int finalFirstResult = calcSquare(firstResult);
        asyncContext.start(() -> {
            req.setAttribute(Constants.KEY_NUMBER, firstArg);
            req.setAttribute(Constants.KEY_RESULT, finalFirstResult);
            asyncContext.dispatch(Constants.SERVLET_VALUE_CALC_CUBE);
        });
    }

    private int calcSquare(final int x) {
        return x == 0 ? x : x * x;
    }

}