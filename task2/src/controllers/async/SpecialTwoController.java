package controllers.async;

import controllers.Utils;
import model.Constants;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = Constants.SERVLET_NAME_CALC_CUBE,
        value = Constants.SERVLET_VALUE_CALC_CUBE)
public class SpecialTwoController extends HttpServlet {
    private final static int TIME_SLEEP = 3000;
    @Override
    protected void doGet(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException {

        String firstArg = (String) req.getAttribute(Constants.KEY_NUMBER);
        int firstResult = (int) req.getAttribute(Constants.KEY_RESULT);
        int secondResult = 0;

        if (Utils.checkOnNullOrEmptyString(firstArg)) {
            secondResult = Integer.parseInt(firstArg);
            secondResult = calcCube(secondResult);
        } else {
            req.setAttribute(Constants.KEY_SPECIAL, Constants.KEY_SPECIAL);
            Utils.forward(req, resp, Constants.JSP_INDEX,
                    "Please input number");
            return;
        }

        req.setAttribute(Constants.KEY_NUMBER, firstArg);
        req.setAttribute(Constants.KEY_RESULT, firstResult);
        req.setAttribute(Constants.KEY_RESULT_TWO, secondResult);
        req.setAttribute(Constants.KEY_SPECIAL, Constants.KEY_SPECIAL);
        Utils.forward(req, resp, Constants.JSP_INDEX);
    }

    private int calcCube(final int x) {
        try {
            Thread.sleep(TIME_SLEEP);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        return x == 0 ? x : x * x * x;
    }
}
