package controllers;

import model.Constants;
import model.beans.Book;
import model.beans.User;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet(name = Constants.SERVLET_NAME_ADD_BOOK,
        value = Constants.SERVLET_VALUE_ADD_BOOK)
public class AddBookController extends BaseController {


    @Override
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        addNewBook(req, resp);
    }

    /**
     * Add new book
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void addNewBook(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        final int userId = obtainUserFromRequest(req).getId();

        final String name = req.getParameter(Book.NAME);
        final String author = req.getParameter(Book.AUTHOR);
        final String description = req.getParameter(Book.DESCRIPTION);
        Book book = new Book(name, author, description, userId);

        if (!Utils.checkOnNullOrEmptyString(name, author, description)) {
            req.setAttribute(Book.BOOK, book);
        }
        getBookDAO().addBook(book);
        req.setAttribute(Book.BOOK, book);
        forward(req, resp, Constants.JSP_INDEX);
    }
}
