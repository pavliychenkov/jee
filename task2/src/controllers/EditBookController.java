package controllers;

import model.Constants;
import model.beans.Book;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * The type Edit book controller.
 */
@WebServlet(name = Constants.SERVLET_NAME_EDIT_BOOK,
        value = Constants.SERVLET_VALUE_EDIT_BOOK)
public class EditBookController extends BaseController {
    @Override
    protected void doPost(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {
        editBook(req, resp);
    }

    /**
     * Edit book on fields: author and description.
     *
     * @param req  the req
     * @param resp the resp
     * @throws ServletException the servlet exception
     * @throws IOException      the io exception
     */
    protected void editBook(
            final HttpServletRequest req,
            final HttpServletResponse resp)
            throws ServletException, IOException {

        String id = req.getParameter(Book.ID);
        int idBook = Integer.parseInt(id);

        Book book = getBookDAO().getBook(idBook);

        String author = req.getParameter(Book.AUTHOR);
        String description = req.getParameter(Book.DESCRIPTION);

        if (Utils.checkOnNullOrEmptyString(author, description)) {
            getBookDAO().updateAuthor(idBook, author);
            getBookDAO().updateDescription(idBook, description);
            book = getBookDAO().getBook(idBook);
            req.setAttribute(Book.BOOK, book);
            forward(req, resp, Constants.JSP_INDEX,
                    Constants.MSG_SUCCESS_BOOK_UPDATED);
            return;
        } else {
            req.setAttribute(Book.BOOK, book);
            forward(req, resp, Constants.JSP_BOOK_PAGE,
                    Constants.ERR_MSG_INVALID_UPDATE_BOOK);
            return;
        }
    }
}
