package controllers;

import model.Constants;
import model.beans.User;
import model.factories.BookDAOFactory;
import model.interfaces.IBookDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

/**
 * The type controllers.Utils.
 */
public final class Utils {
    private Utils() {
    }

    /**
     * Check on null or empty string boolean.
     *
     * @param strings the strings
     * @return the boolean
     */
    public static boolean checkOnNullOrEmptyString(final String... strings) {
        for (String s : strings) {
            s = s.trim();
            if (s == null || "".equals(s)) {
                return false;
            }
        }
        return true;
    }

    /**
     * Get book dao from BookDAOFactory
     *
     * @return the book dao
     */
    public static IBookDAO getBookDAO() {
        return BookDAOFactory.getBookDAO(Constants.BOOK_DAO);
    }

    public static void forward(final HttpServletRequest request,
                               final HttpServletResponse response,
                               final String forwardStr)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher(forwardStr);
        dispatcher.forward(request, response);
    }

    public static void forward(final HttpServletRequest request,
                               final HttpServletResponse response,
                               final String forwardStr,
                               final String msg)
            throws ServletException, IOException {
        request.setAttribute(Constants.KEY_MSG, msg);
        forward(request, response, forwardStr);
    }
}
