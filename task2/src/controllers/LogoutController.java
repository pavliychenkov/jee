package controllers;

import model.Constants;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = Constants.SERVLET_NAME_LOGOUT,
        value = Constants.SERVLET_VALUE_LOGOUT)
public class LogoutController extends BaseController {
    @Override
    protected void doGet(final HttpServletRequest req,
                         final HttpServletResponse resp)
            throws ServletException, IOException {
        final HttpSession session = req.getSession();
        if (session != null) {
            session.invalidate();
            forward(req, resp, Constants.JSP_LOGIN_PAGE,
                    Constants.MSG_LOGOUT);
            return;
        }
        forward(req, resp, Constants.JSP_LOGIN_PAGE);
    }
}
