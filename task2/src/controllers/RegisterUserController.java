package controllers;

import model.Constants;
import model.beans.User;
import model.factories.UserDAOFactory;
import model.interfaces.IUserDAO;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

@WebServlet(name = Constants.SERVLET_NAME_REGISTER_USER,
        value = Constants.SERVLET_VALUE_REGISTER_USER)
public class RegisterUserController extends BaseController {

    @Override
    protected void doPost(final HttpServletRequest req,
                          final HttpServletResponse resp)
            throws ServletException, IOException {
        registerUser(req, resp);
    }

    /**
     * register new user.
     *
     * @param req
     * @param resp
     * @throws ServletException
     * @throws IOException
     */
    protected void registerUser(final HttpServletRequest req,
                                final HttpServletResponse resp)
            throws ServletException, IOException {
        HttpSession session = req.getSession();
        final String login = req.getParameter(User.LOGIN);
        final String password = req.getParameter(User.PASSWORD);
        final String firstName = req.getParameter(User.FIRST_NAME);
        final String secondName = req.getParameter(User.SECOND_NAME);

        if (!Utils.checkOnNullOrEmptyString(login, password,
                firstName, secondName)) {
            forward(req, resp, Constants.JSP_PAGE_REGISTER,
                    Constants.ERR_MSG_INPUT_DATA_EMPTY);
            return;
        }

        final User user = getUserDAO().registerUser(login, password,
                firstName, secondName);

        if (user != null) {
            session.setAttribute(Constants.KEY_USER, user);
            req.setAttribute(Constants.KEY_LOGIN, login);
            req.setAttribute(Constants.KEY_PASSWORD, password);
            forward(req, resp, Constants.SERVLET_VALUE_LOGIN);
            return;
        } else {
            forward(req, resp, Constants.JSP_PAGE_REGISTER,
                    "Sorry but user with login(" + login + ") already exist");
            return;
        }
    }
}
