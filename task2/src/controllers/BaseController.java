package controllers;

import model.Constants;
import model.beans.User;
import model.factories.UserDAOFactory;
import model.interfaces.IBookDAO;
import model.interfaces.IUserDAO;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import java.io.IOException;

public abstract class BaseController extends HttpServlet {
    private IBookDAO bookDAO;
    private IUserDAO userDAO = UserDAOFactory.getDAO(Constants.USER_DAO);

    @Override
    public void init() throws ServletException {
        super.init();
        bookDAO = (IBookDAO) getServletContext()
                .getAttribute(Constants.KEY_DAO_BOOK);
    }

    protected User obtainUserFromRequest(final HttpServletRequest req) {
        return (User) req.getSession().getAttribute(Constants.KEY_USER);
    }

    protected void forward(final HttpServletRequest request,
                           final HttpServletResponse response,
                           final String forwardStr)
            throws ServletException, IOException {
        RequestDispatcher dispatcher = request.getRequestDispatcher(forwardStr);
        dispatcher.forward(request, response);
    }

    protected void forward(final HttpServletRequest request,
                           final HttpServletResponse response,
                           final String forwardStr,
                           final String msg)
            throws ServletException, IOException {
        request.setAttribute(Constants.KEY_MSG, msg);
        forward(request, response, forwardStr);
    }

    protected IBookDAO getBookDAO() {
        return bookDAO;
    }

    protected IUserDAO getUserDAO() {
        return userDAO;
    }
}
