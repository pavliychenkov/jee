package listeners;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;
import java.util.logging.Logger;

@WebListener
public class SessionListener implements HttpSessionListener {
    private final static Logger LOGGER =
            Logger.getLogger(SessionListener.class.getName());
    private final static String EMPTY_PREFIX = "    ";

    @Override
    public void sessionCreated(final HttpSessionEvent httpSessionEvent) {
        getInfoTimeEvent(httpSessionEvent, Status.CREATED);
    }

    @Override
    public void sessionDestroyed(final HttpSessionEvent httpSessionEvent) {
        getInfoTimeEvent(httpSessionEvent, Status.DESTROYED);
    }

    private void getInfoTimeEvent(final HttpSessionEvent httpSessionEvent,
                                  final Status status) {
        LOGGER.info(EMPTY_PREFIX
                + System.currentTimeMillis()
                + " " + status.toString().toLowerCase() + " session "
                + httpSessionEvent.getSession().getId());
    }

    private static enum Status {
        CREATED, DESTROYED
    }
}