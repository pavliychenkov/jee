package listeners;

import model.Constants;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import javax.servlet.ServletRegistration;
import javax.servlet.annotation.WebListener;
import java.util.Map;
import java.util.logging.Logger;

@WebListener
public class Listener implements ServletContextListener {
    private static final Logger LOGGER = Logger.getLogger(Listener.class.getName());

    public Listener() {
    }

    @Override
    public void contextInitialized(
            final ServletContextEvent servletContextEvent) {
        ServletContext context = servletContextEvent.getServletContext();

        LOGGER.info(context.getServerInfo());
        LOGGER.info(context.getServletContextName());
        LOGGER.info(Constants.MSG_AVAILABLE_SERVLET);
        Map<String, ? extends ServletRegistration> registrations
                = context.getServletRegistrations();
        registrations.entrySet().stream()
                .forEach(entry -> LOGGER.info(
                        entry.getKey() + " = " + entry.getValue()));
    }

    @Override
    public void contextDestroyed(final ServletContextEvent servletContextEvent) {
    }
}
