package listeners;

import javax.servlet.AsyncEvent;
import javax.servlet.annotation.WebListener;
import java.io.IOException;
@WebListener
public class MyAsyncListener implements javax.servlet.AsyncListener {
    @Override
    public void onComplete(AsyncEvent asyncEvent) throws IOException {
        System.out.println("complete " + Thread.currentThread().getName());
    }

    @Override
    public void onTimeout(AsyncEvent asyncEvent) throws IOException {
        System.out.println("on time");
    }

    @Override
    public void onError(AsyncEvent asyncEvent) throws IOException {
        System.out.println(" on error");
    }

    @Override
    public void onStartAsync(AsyncEvent asyncEvent) throws IOException {
        System.out.println("start" + Thread.currentThread().getName());
    }
}
