package wrappers;

import model.Constants;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletRequestWrapper;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * The type Xss wrapper request.
 */
public class XSSWrapperRequest extends HttpServletRequestWrapper {
    private static final Pattern PATTERN = Pattern.compile("<script>(.*?)</script>", Pattern.CASE_INSENSITIVE);

    /**
     * Instantiates a new Xss wrapper request.
     *
     * @param request the request
     */
    public XSSWrapperRequest(final HttpServletRequest request) {
        super(request);
    }

    @Override
    public String[] getParameterValues(final String name) {
        String[] values = super.getParameterValues(name);
        String[] resultValues = new String[values.length];

        if (values == null) {
            return null;
        }

        for (int i = 0; i < values.length; i++) {
            resultValues[i] = checkXSS(values[i]);
        }

        return resultValues;
    }

    @Override
    public String getParameter(final String name) {
        String resultValue = super.getParameter(name);
        return checkXSS(resultValue);
    }

    /**Check string for XSS attack
     * @param param
     * @return
     */
    private String checkXSS(final String param) {
        String name = param;
        if (name == null) {
            return null;
        }
        Matcher matcher = PATTERN.matcher(name);
        if (matcher.find()) {
            return Constants.ERR_MSG_XSS_WRAPPER;
        }
        return name;
    }
}
