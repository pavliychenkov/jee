<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<html>
  <head>
    <title>Task_1</title>
  </head>
  <body>
    <form action="/getBooksController" method="get" >
      <input type="submit" value="get books from BD">
    </form>
  <c:if test="${not empty books}">
    <h2>List of books from BD</h2>
    <form action="/chooseBook" method="get" name="action">
      <c:forEach  items="${books}" var="book">
        <b>Название: </b> ${book.name} <br>
        <b>Автор:    </b> ${book.author} <br>
        <b>Описание: </b> ${book.description} <br><br>
      </c:forEach>
    </form>

  </c:if>
  </body>
</html>
