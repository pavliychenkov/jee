package model.service;

import model.Constants;

import java.sql.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class DBManager {
    Logger LOGGER = Logger.getLogger(DBManager.class.getName());
    private Connection connection;
    private PreparedStatement statement;

    public DBManager() {
        createConnection();
    }

    private Connection createConnection(){
        try {
            Class.forName(Constants.BD_CLASS_NAME);
            connection = DriverManager.getConnection(Constants. DB_URL,Constants.DB_USER, Constants.DB_PSWRD);
        } catch (ClassNotFoundException | SQLException e) {
            LOGGER.log(Level.SEVERE, e.getMessage());
        }
        return connection;
    }

    public Connection getConnection() {
        return connection;
    }

    public PreparedStatement getPreparedStatement(String sql) throws SQLException {
        return statement = connection.prepareStatement(sql);
    }
}
